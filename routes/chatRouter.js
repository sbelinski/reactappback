const express = require('express');
const router = express.Router();
const chatController = require('../controllers/chatController');
const passport = require('passport');

router.use(passport.authenticate('jwt', {session: false}));

router.get('/room', chatController.getRoom);
router.post('/room', chatController.createRoom);
router.get('/messages/:roomId', chatController.getMessages);
router.get('/myRooms', chatController.getMyRooms);


module.exports = router;
