const mongoose = require('mongoose');

const {Schema} = mongoose;

const roomSchema = new Schema({
    name: {type: String, require: true},
}, {versionKey: false});

const roomModel = mongoose.model('room', roomSchema);

module.exports = roomModel;
