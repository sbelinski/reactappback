const mongoose = require('mongoose');

const {Schema} = mongoose;

const messageSchema = new Schema({
    authorId: {type:  Schema.Types.ObjectId, require: true},
    author: {type:  String, require: true},
    roomId: {type:  Schema.Types.ObjectId, require: true},
    text: {type:  String, require: true},
    createdAt: {type: String}
}, {versionKey: false});

const messageModel = mongoose.model('message', messageSchema);

module.exports = messageModel;
