const messageModel = require('../models/message');
const userModel = require('../models/user');

class socketService {
    onMessage(socket, data){
        userModel.findOne({_id: data._id})
            .then(
                user => {
                    messageModel.create({
                        roomId: data.room,
                        authorId: data._id,
                        author: user.login,
                        text: data.message,
                        createdAt: data.createdAt,
                    })
                        .then(
                            message => {
                                socket.broadcast.to(data.room).emit('message', message);
                            }
                        )
                });
    }

    joinRoom(socket, data){
        const room = data.room;
        socket.join(data.room);
        userModel.updateOne({_id: data._id}, {$push: {rooms: room}})
            .then(
                data => {
                console.log(data)}
            );
    }

    openRoom(socket, data){
        socket.join(data.room);
    }

    newChat(socket, data){
        const chat = data.chat;
        socket.broadcast.emit(
            'new_chat',
            {
                chat
            }
        )
    }

    async leaveChat(socket, data){
        console.log(data);
        socket.leave(data.room);
        await userModel.findOneAndUpdate( {_id: data._id},{
            $pull: {rooms: data.room}
        })
    }

}


module.exports = new socketService;
