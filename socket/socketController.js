const socketService = require('./socketService');

class SocketServerClass {
    createSocketServer(server){
        const io = require('socket.io')(server);
        io.on('connection',(socket)=>{
            console.log('One connection');

            socket.on('open_room', (data)=>{
                socketService.openRoom(socket, data)
            });

            socket.on('join', (data)=> {
                socketService.joinRoom(socket, data);
            });

            socket.on('message', (data)=>{
                socketService.onMessage(socket, data)
            });

            socket.on('new_chat', (data)=> {
                socketService.newChat(socket, data);
            });

            socket.on('leave', (data)=> {
                socketService.leaveChat(socket, data);
            });

            socket.on('disconnect', ()=> {
                socket.leaveAll();
                console.log('Someone disconnected')
            })
        });
    }
}

module.exports = new SocketServerClass;
