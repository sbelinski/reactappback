const UserModel = require('../models/user');

class UserAccountService {
    getUserInfo(req, res) {
        UserModel.findOne({_id: req._id}).then(data => {
            res.send(data);
        });
    }

    updateUserInfo(req, res) {
        UserModel.updateOne({login: req.body.login}, req.body).then((data) => res.send(data));
    }

}

let userAccountService = new UserAccountService();
module.exports = userAccountService;
