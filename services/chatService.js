const roomModel = require('../models/room');
const messageModel = require('../models/message');
const userModel = require('../models/user');

class ChatService {
    getRooms(req, res, next){
        roomModel.find({})
            .then(
                data => {
                    res.send(data)
                }
            )
            .catch(
                err => {
                    console.log(err)
                }
            )
    }

    createRoom(req, res, next){
        roomModel.create(req.body)
            .then(data => {
                res.send(data);
            })
            .catch(
                err => {
                    console.log(err)
                }
            )
    }

    getMessages(req, res, next){
        messageModel.find({roomId: req.params.roomId})
            .then(
                data => {
                    res.send(data)
                }
            );
    }

    getMyRooms(req, res, next){
        let rooms = [];
        userModel.findOne({_id: req._id})
            .then(
                async user => {
                    for (let room of user.rooms){
                        await roomModel.findOne({_id: room})
                            .then(
                                data => {
                                    rooms.push(data)
                                }
                            );
                    }
                    res.send(rooms)
                }
            )
    }
}

let chatService = new ChatService();
module.exports = chatService;
