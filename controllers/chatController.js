const chatService = require('../services/chatService');

class ChatController {
    getRoom(req, res, next){
        chatService.getRooms(req, res, next)
    }

    createRoom(req, res, next){
        chatService.createRoom(req, res, next)
    }

    getMessages(req, res, next){
        chatService.getMessages(req, res, next)
    }

    getMyRooms(req, res, next){
        chatService.getMyRooms(req, res, next)
    }
}

const chatController = new ChatController();
module.exports = chatController;
